#include "snobject.h"

#ifndef SNAIKS_HLD_H_
#define SNAIKS_HLD_H_


// Analog Hold-Gate
class SnHLD:public SnObject
{
public:
	SnHLD(int id, const char* const name=0);
	void update();
	Sn::ObjType type();


	snanalog_t In;

	bool Hld;  // If true, hold input value
	bool R;

	snanalog_t Out;
private:
	bool lastHld;
};

#endif
