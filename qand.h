#include "snobject.h"

#ifndef SNAIKS_QAND_H_
#define SNAIKS_QAND_H_

class SnDL;

//	And with 4 inputs
class SnQAND:public SnObject
{
public:
	SnQAND(int id, const char* const name=0);
	void update();
	Sn::ObjType type();

	bool In1;
	bool In2;
	bool In3;
	bool In4;

protected:
	SnDL* dl;
};


#endif
