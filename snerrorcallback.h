#ifndef SNERRORCALLBACK_H
#define SNERRORCALLBACK_H

// TODO 5: implement as SnaiksErrorCallback function pointer,
// which can be optional.

// this function must be defined in the main programm by the end-user of the Snaiks-Lib
void SnaiksErrorCallback(const char* const msg,
								   const char* const file, int line);

#endif // SNERRORCALLBACK_H
