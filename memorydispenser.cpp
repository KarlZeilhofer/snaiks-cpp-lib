#include "memorydispenser.h"
#include "snobject.h"

#ifdef ARDUINO_DEBUG
#include <Arduino.h>
#endif

extern uint8_t snaiks_memory_heap[];

// initializers for static memebers:
uint8_t* MemoryDispenser::memory = 0;			// must be set with init()!
size_t MemoryDispenser::size = 0;				// must be set with init()!
uint8_t* MemoryDispenser::nextFreeAddress = 0;	// must be set with init()!

void* MemoryDispenser::begin()
{
    return (void*)(memory);
}

void* MemoryDispenser::end()
{
    return (void*)(memory + size);
}

void MemoryDispenser::init(uint8_t *staticMemory, size_t size)
{
	memory = staticMemory;
	nextFreeAddress = memory;
	MemoryDispenser::size = size;
}

void* MemoryDispenser::get(size_t bytes)
{
#ifdef ARDUINO_DEBUG
		Serial.print("MD::get(");
		Serial.print(bytes);
		Serial.println(")");
#endif

	if(nextFreeAddress == 0){
		SnaiksErrorCallback("nullpointer", __FILE__, __LINE__);
	}

    if(bytes==0){
        return nextFreeAddress;
    }


	// Do a pre-alignment depending on bytes

	// this should save some memory, for successive small alignments!
	// if bytes == 1: do nothing
	// if bytes == 2: forward to next multiple of 2
	// if bytes == 3 or 4: forward to next multiple of 4 (only on 32-bit plattforms)
	// if bytes >= 5: forward to next multiple of 8 (only on 64-bit plattforms)

	// do nothing for bytes == 1;
	uint8_t intSize = bytes; // for the case 1 and 2;
	if(bytes == 1 || bytes == 2  || sizeof(size_t) == 2){
		// do nothing here
	}else if(bytes == 3 || bytes == 4 || sizeof(size_t) == 4){
		intSize = 4;
	}else if(bytes >= 5){ // 64-bit plattforms
		intSize = 8;
	}

	uint8_t stuffBytes = (intSize-((size_t)nextFreeAddress%intSize))%intSize;

	// keep 32-bit or 64-bit alignment!!!
	uint8_t* alignedAddress = (uint8_t*)
			((size_t)(nextFreeAddress) + (stuffBytes));

	if((size_t)alignedAddress%intSize){
		SnaiksErrorCallback("bad alignment", __FILE__, __LINE__);
		return 0;
	}

	nextFreeAddress = alignedAddress + bytes;
	if(nextFreeAddress >= end()){
		SnaiksErrorCallback("bad alloc", __FILE__, __LINE__);
		return 0;
    }else{
		return (void*)alignedAddress;
    }
}


size_t MemoryDispenser::remainingSize()
{
    return (uint8_t*)end() - nextFreeAddress;
}

size_t MemoryDispenser::totalSize()
{
	return size;
}

size_t MemoryDispenser::dispensedSize()
{
	return nextFreeAddress - memory;
}

void MemoryDispenser::reset()
{
    nextFreeAddress = memory;
}
