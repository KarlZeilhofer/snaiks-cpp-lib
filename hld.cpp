#include "hld.h"
#include "snpin.h"



SnHLD::SnHLD(int id, const char * const name)
	:SnObject(id, name), lastHld(false)
{
	alloc(3,1,0,0);

	addInput(snaiksm(SnPin<snanalog_t>(11, &In)));
	addInput(snaiksm(SnPin<bool>(12, &Hld)));
	addInput(snaiksm(SnPin<bool>(13, &R)));

	addOutput(snaiksm(SnPin<snanalog_t>(51, &Out)));
}

void SnHLD::update()
{
	if((Hld && !lastHld) || !Hld){ // rising edge
		Out = In;
	}
	if(R){
		Out = 0;
	}

	lastHld = Hld;
}

Sn::ObjType SnHLD::type()
{
	return Sn::HLD;
}

