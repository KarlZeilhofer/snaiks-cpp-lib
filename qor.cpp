
#include "qor.h"
#include "dl.h"
#include "snpin.h"

SnQOR::SnQOR(int id, const char* const name)
	:SnQAND(id, name)
{
}

void SnQOR::update()
{
	dl->In = In1 || In2 || In3 || In4;
	dl->update();
}

Sn::ObjType SnQOR::type()
{
	return Sn::QOR;
}

