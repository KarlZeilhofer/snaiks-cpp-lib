#ifndef SNPIN_H
#define SNPIN_H

#include "snobject.h"
#include "config.h"

// support lagacy tpyes:
#define SnPinBool SnPin<bool>
#define SnPinDouble SnPin<snanalog_t>

// TODO 5: for next version of snaiks, implement SnPin as template
// class SnPinInterface
// class SnPin<typename>: public SnPinInterface
// with special implementations for typical types (bool, snanalog_t)
class SnPinInterface
{
public:
	SnPinInterface(uint8_t number); // TODO 5: enable pin names here with preprocessor macro
	static int globalPinsCounter;
	uint8_t number; // inique identifier within a SnObject's container
#ifndef SNAIKS_NAMELESS_PINS
	char* name;
#endif

	virtual Sn::SignalType type() const =0;
	virtual uint8_t typeSize() const = 0;

	virtual void* outputAddress() const = 0;
	virtual void* sourceAddress() const = 0;
	virtual void setSource(void* s) = 0;
	virtual void sample() = 0;
	virtual int print(char* destStr, size_t maxLen);
};

template<class T>
class SnPin : public SnPinInterface
{
public:
	SnPin(uint8_t number) : SnPinInterface(number) {
		// when no memory is provided, we have to allocate it:
#ifdef USE_PLACEMENT_NEW
		output = (T*) (MemoryDispenser::get(sizeof(T)));
#else
		output = new T();
#endif
		*output = (T)0; // default value
		source = output; // by default set the source to our output
			// --> no nullpointer-exception, if sources isn't defined.
	}
	SnPin(uint8_t number, T* staticMemory, T* sourceAddress=0) : SnPinInterface(number)
	{
		if(staticMemory){
			output = staticMemory;
			*output = (T)0; // default value
			if(sourceAddress){
				source = sourceAddress;
			}else{
				source = output; // by default set the source to our output
				// --> no nullpointer-exception, if sources isn't defined.
			}
		}else{
			(*SnaiksErrorCallback)("nullpointer in SnPin-constructor", __FILE__, __LINE__);
		}
	}

	//int print(char* destStr, size_t maxLen) const;

	T* source;
	T* output;

	Sn::SignalType type() const;
	uint8_t typeSize() const{
		return sizeof(T);
	}


	void* outputAddress() const{
		return (void*) output;
	}
	void* sourceAddress() const{
		return (void*) source;
	}
	void setSource(void* s){
		source = (T*) s; // typecast
	}

	void sample(){
		*output = *source; // copy
	}
};

#endif // SNPIN_H
