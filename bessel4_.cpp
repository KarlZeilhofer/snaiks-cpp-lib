#include "bessel4_.h"
#include "snpin.h"
#include "snproperty.h"

SnBESSEL4_::SnBESSEL4_(int id, const char * const name, const char * const props)
    :SnObject(id, name)
{
	alloc(2,1,0,1);

	addInput(snaiksm(SnPin<snanalog_t>(11, &In)));
	addInput(snaiksm(SnPin<bool>(12, &R)));

	addOutput(snaiksm(SnPin<snanalog_t>(51, &Out)));

	addPropterty(snaiksm(SnProperty<snanalog_t>("UpdateRateDivider", &UpdateRateDivider)));

	UpdateRateDivider = 1; // default value
	setProps(props);
	updateCounter=0;
}

void SnBESSEL4_::update()
{
	// TODO 2: implement update-rate divider
	// here is a undetectable bug with NFS battery analyzer (memory overflow?)
//	UpdateRateDivider = 1;

//	updateCounter++;
//	if(updateCounter >= UpdateRateDivider){
//		updateCounter = 0;
//	}

	if(!R){
//		if(updateCounter != 0){
			Out = f.step(In);
//		}
	}else{ // reset is transparent
		f.reset();
		Out=0;
	}
}

Sn::ObjType SnBESSEL4_::type()
{
	return Sn::BESSEL4_;
}


