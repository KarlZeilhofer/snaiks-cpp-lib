#include "abs.h"
#include "snpin.h"

SnABS::SnABS(int id, const char * const name)
	:SnObject(id, name)
{
	alloc(1,1,0,0);

	addInput(snaiksm(SnPin<snanalog_t>(11, &In)));

	addOutput(snaiksm(SnPin<snanalog_t>(51, &Out)));
}

void SnABS::update()
{
	if(In < 0){
		Out = -In;
	}else{
		Out = In;
	}
}

Sn::ObjType SnABS::type()
{
	return Sn::ABS;
}
