#include "snobject.h"

#ifndef SNAIKS_DIV_H_
#define SNAIKS_DIV_H_


// Divide
class SnDIV:public SnObject
{
public:
	SnDIV(int id, const char* const name=0, const char * const props=0);
	void update();
	Sn::ObjType type();


	snanalog_t In1;
	snanalog_t In2;

	snanalog_t Out;

	snanalog_t DivByZeroValue; // Output value, when division by zero
};



#endif
