#ifndef PS_H
#define PS_H

#include "snobject.h"

// Property Setter
// TODO 3: finish implementation of propterty-setter
class SnPS : public SnObject
{
public:
	// example
	// addObj(snaiksm(SnPS(1, "Time Window Property Setter",
	//			  getObj(Sn::AD, 1), "Delay")));

	SnPS(int id, const char* const name=0, const char * const props=0);
	SnPS(int id, const char* const name,
		 SnObject* remoteObj, const char* const remotePropertyName);

	void update();
	Sn::ObjType type();

	snanalog_t In; // TODO 3: make it a template type

	SnObject* remoteObj;
	char remoteProps[40]; // TODO 5: check for size
	int remotePropsNameLen; // e.g. strlen("Period;")
		// the actual value will be appended to remoteProps each update
};

#endif // PS_H
