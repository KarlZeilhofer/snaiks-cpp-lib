#include "mul.h"
#include "snpin.h"

SnMUL::SnMUL(int id, const char * const name)
	:SnObject(id, name)
{
	alloc(2,1,0,0);

	addInput(snaiksm(SnPin<snanalog_t>(11, &In1)));
	addInput(snaiksm(SnPin<snanalog_t>(12, &In2)));

	addOutput(snaiksm(SnPin<snanalog_t>(51, &Out)));
}

void SnMUL::update()
{
	Out = In1*In2;
}

Sn::ObjType SnMUL::type()
{
	return Sn::MUL;
}
