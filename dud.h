#ifndef DUD_H
#define DUD_H

#include "snobject.h"

class SnDUD : public SnObject
{
public:
	SnDUD(int id, const  char* const name);
	void update();
	Sn::ObjType type();
	bool isTransparent();

public:
	bool In;
	bool Out;
};

#endif // DUD_H
