#include "dout.h"
#include "snpin.h"

SnDOUT::SnDOUT(int id, const char* const name)
	:SnObject(id, name)
{
	alloc(1,0,0,0);
	addInput(snaiksm(SnPin<bool>(11, &In)));
}

void SnDOUT::update()
{

}

Sn::ObjType SnDOUT::type()
{
	return Sn::DOUT;
}
