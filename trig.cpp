#include "trig.h"
#include "snpin.h"

SnTRIG::SnTRIG(int id, const char* const name)
	:SnObject(id, name)
{
	alloc(0,1,0,0);
	addOutput(snaiksm(SnPin<bool>(51, &Out)));

	triggered = false;
}

void SnTRIG::update()
{
	if(triggered){
		Out = true;
		triggered = false; // reset falg
	}else{
		Out = false; // reset Output
	}
}

Sn::ObjType SnTRIG::type()
{
	return Sn::TRIG;
}

void SnTRIG::trigger()
{
	triggered = true;
}
