#ifndef SNAUD_H
#define SNAUD_H

#include "snobject.h"

class SnAUD : public SnObject
{
public:
	SnAUD(int id, const  char* const name);
	void update();
	Sn::ObjType type();
	bool isTransparent();

public:
	snanalog_t In;
	snanalog_t Out;
};

#endif // SNAUD_H
