#include "cmp.h"
#include "snpin.h"
#include "dl.h"

SnCMP::SnCMP(int id, const char* const name)
	:SnObject(id, name)
{

	alloc(2,4,0,0);
	addInput(snaiksm(SnPin<snanalog_t>(11, &In1)));
	addInput(snaiksm(SnPin<snanalog_t>(12, &In2)));

	dl = snaiksm(SnDL(1));
	//addObj(dl); // don't add, since we update it manually

	addOutput(snaiksm(SnPin<bool>(51, &dl->Q)));
	addOutput(snaiksm(SnPin<bool>(52, &dl->rising)));
	addOutput(snaiksm(SnPin<bool>(53, &dl->falling)));
	addOutput(snaiksm(SnPin<bool>(54, &dl->Qn)));
}

void SnCMP::update()
{
	if(In1 >= In2){
		dl->In = true;
	}else{
		dl->In = false;
	}

	dl->update();
}

Sn::ObjType SnCMP::type()
{
	return Sn::CMP;
}

