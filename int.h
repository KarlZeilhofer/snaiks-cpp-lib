#include "snobject.h"

#ifndef SNAIKS_INT_H_
#define SNAIKS_INT_H_


// Integrator
class SnINT:public SnObject
{
public:
	SnINT(int id, const char* const name=0);
	void update();
	Sn::ObjType type();

	snanalog_t In;
	bool R;

	snanalog_t Out;
private:
	snanalog_t lastIn;
};


#endif
