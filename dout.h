#include "snobject.h"

#ifndef SNAIKS_DOUT_H_
#define SNAIKS_DOUT_H_


// Digital Line
class SnDOUT:public SnObject
{
public:
	SnDOUT(int id, const char* const name=0);

	void update();
	Sn::ObjType type();

	bool In;
};



#endif
