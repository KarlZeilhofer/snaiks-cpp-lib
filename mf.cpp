#include "mf.h"
#include "dl.h"
#include "snpin.h"
#include "snproperty.h"

SnMF::SnMF(int id, const char* const name, const char* const props)
	:SnObject(id, name)
{
	counterTicks = 0;

	alloc(2, 4, 0, 1);

	addInput(snaiksm(SnPin<bool>(11, &S)));
	addInput(snaiksm(SnPin<bool>(12, &R)));

	dl = snaiksm(SnDL(1));

	// see SnDAND for more explanation
	addOutput(snaiksm(SnPin<bool>(51, &dl->Q)));
	addOutput(snaiksm(SnPin<bool>(52, &dl->rising)));
	addOutput(snaiksm(SnPin<bool>(53, &dl->falling)));
	addOutput(snaiksm(SnPin<bool>(54, &dl->Qn)));

	addPropterty(snaiksm(SnProperty<snanalog_t>("Period", &Period)));
	Period = 1.0; // 1s is default

	setProps(props);
	counterTicks = 0;
}

void SnMF::update()
{
	uint64_t periodTicks = Period*(1.0/SnObject::GlobalSampleTime);

	if(dl->Q){ // count only when set (due to possible overflow problem)
		counterTicks++;
	}

	if(S){
		counterTicks=0;
		dl->In = true;
	}
	// reset dominates!
	if(R || counterTicks>=periodTicks){
		dl->In = false;
	}
	dl->update();
}

Sn::ObjType SnMF::type()
{
	return Sn::MF;
}

