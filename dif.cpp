#include "dif.h"
#include "snpin.h"

SnDIF::SnDIF(int id, const char * const name)
	:SnObject(id, name)
{
	alloc(2,1,0,0);

	addInput(snaiksm(SnPin<snanalog_t>(11, &Plus)));
	addInput(snaiksm(SnPin<snanalog_t>(12, &Minus)));

	addOutput(snaiksm(SnPin<snanalog_t>(51, &Out)));
}

void SnDIF::update()
{
	Out = Plus-Minus;
}

Sn::ObjType SnDIF::type()
{
	return Sn::DIF;
}

#include "dif.h"
