#include "dor.h"
#include "dl.h"
#include "snpin.h"

SnDOR::SnDOR(int id, const char* const name)
	:SnDAND(id, name)
{
	// derived from DAND!
}

void SnDOR::update()
{
	dl->In = In1 || In2;
	dl->update();
}

Sn::ObjType SnDOR::type()
{
	return Sn::DOR;
}
