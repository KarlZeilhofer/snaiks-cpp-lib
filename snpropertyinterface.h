#ifndef SNPROPERTYINTERFACE_H
#define SNPROPERTYINTERFACE_H


/**
 * @brief The SnsPropertyInterface is the abstract super class to SnProperty
 *
 * It provides a common interface to a CLI to read and set values.
 * It is also used to collect properties, type-independent, in a SnContainer
 */

class SnPropertyInterface
{
public:
	static int globalCounter;
public:
	virtual char* getValueAsVariant() = 0; // pure virtual
	virtual void setValueByVariant(const char* variant, bool* ok=0) = 0; // pure virtual
	char* getName()const {return name;}

protected:
	char* name; /// name of the property
};

#endif // SNPROPERTYINTERFACE_H
