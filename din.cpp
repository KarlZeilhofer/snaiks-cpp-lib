#include "din.h"
#include "snproperty.h"
#include "snpin.h"


SnDIN::SnDIN(int id, const char* const name, const char* const props)
	:SnObject(id, name)
{
	alloc(0,1,0,1);
	addOutput(snaiksm(SnPin<bool>(51, &Out)));

	addPropterty(snaiksm(SnProperty<bool>("InitValue", &InitValue)));

	InitValue = false;
	setProps(props);
	Out = InitValue;
}

void SnDIN::update()
{
	SnObject::update(); // sample output-pin
		// (for the case an external value is given as source for our output pin)
}

Sn::ObjType SnDIN::type()
{
	return Sn::DIN;
}
