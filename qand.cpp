#include "qand.h"
#include "snpin.h"
#include "dl.h"

SnQAND::SnQAND(int id, const char * const name)
	:SnObject(id, name)
{
	In1 = false;
	In2 = false;
	In3 = false;
	In4 = false;

	alloc(4,4,0,0);

	addInput(snaiksm(SnPin<bool>(11, &In1)));
	addInput(snaiksm(SnPin<bool>(12, &In2)));
	addInput(snaiksm(SnPin<bool>(13, &In3)));
	addInput(snaiksm(SnPin<bool>(14, &In4)));

	dl = snaiksm(SnDL(1));
	//addObj(dl); // don't add, since we update it manually

	// see SnDAND for more explanation
	addOutput(snaiksm(SnPin<bool>(51, &dl->Q)));
	addOutput(snaiksm(SnPin<bool>(52, &dl->rising)));
	addOutput(snaiksm(SnPin<bool>(53, &dl->falling)));
	addOutput(snaiksm(SnPin<bool>(54, &dl->Qn)));
}

void SnQAND::update()
{
	dl->In = In1 && In2 && In3 && In4;
	dl->update();
}

Sn::ObjType SnQAND::type()
{
	return Sn::QAND;
}
