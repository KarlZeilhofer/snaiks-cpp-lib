#ifndef SNAIKSLIB_H
#define SNAIKSLIB_H

// TODO 1: check initialization of each pin!
//  pin-constructors set them allways to zero

#include "config.h"
#include "snobject.h"
#include "snpin.h"
#include "snproperty.h"
#include "snunittest.h"

// all time-related objects:
#include "ad.h"		// activation delay, TODO 5: "output" precision for AVR-GCC
#include "bessel4_.h" // a 4th order bessel low pass filter with update divider
#include "int.h"	// numeric integrator, TODO 2: precision for AVR-GCC
#include "mf.h"		// mono flop, TODO 5: precision for AVR-GCC
#include "timer.h"	// pausable time counter, TODO 5: output precision for AVR-GCC
#include "srl.h"	// slew rate limitation


// TODO 5: list by numeric and logic:
#include "abs.h"	// numeric absolute value |x|
#include "ain.h"	// analog input
#include "aout.h"	// analog output
#include "cb.h"		// custom block
#include "cmp.h"	// comparator of 2 analog values
#include "damx.h"	// dual analog multiplexer 2x in, 1x out
#include "dand.h"	// dual logic AND
#include "ddmx.h"	// dual digital multiplexer 2x in, 1x out
#include "dif.h"	// numeric difference (subtract)
#include "din.h"	// digital input
#include "div.h"	// numeric divide
#include "dl.h"		// digital line (with Q, rising, falling, Qn)
#include "dor.h"	// dual logic OR
#include "dout.h"	// digital output
#include "ff.h"		// flip flop with Set and Reset
#include "hld.h"	// analog hold gate
#include "max.h"	// numeric maximum of 2 inputs
#include "min.h"	// numeric minimum of 2 inputs
#include "mul.h"	// numeric multiply
#include "qand.h"	// quad logic AND (4 inputs)
#include "qor.h"	// quad logic OR (4 inputs)
#include "sat.h"	// analog saturation
#include "sc.h"		// sign changer (multiply with -1)
#include "sign.h"	// signum-function of numeric value (outputs +1, 0, -1)
#include "st.h"		// schmitt trigger
#include "sum.h"	// numeric addition of 2 numbers
#include "trig.h"	// trigger, generates a high-pulse on the output, provides a trigger() member function.
#include "ps.h"		// property setter // TODO 3: property setter

// unit delay objects, used for breaking up a algebraic loops or building digital filters
#include "dud.h"	// digital unit delay
#include "aud.h"	// analog unit delay

#include "snerrorcallback.h"


#endif // SNAIKSLIB_H
