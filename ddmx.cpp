#include "ddmx.h"
#include "snpin.h"

SnDDMX::SnDDMX(int id, const char* const name)
	:SnObject(id, name)
{
	In0 = false;
	In1 = false;
	Select = false;

	alloc(3,1,0,0);

	addInput(snaiksm(SnPin<bool>(11, &In0)));
	addInput(snaiksm(SnPin<bool>(12, &In1)));
	addInput(snaiksm(SnPin<bool>(13, &Select)));

	addOutput(snaiksm(SnPin<bool>(51, &Out)));
	Out = false;
}

void SnDDMX::update()
{
	if(Select){
		Out = In1;
	}else{
		Out = In0;
	}
}

Sn::ObjType SnDDMX::type()
{
	return Sn::DDMX;
}
