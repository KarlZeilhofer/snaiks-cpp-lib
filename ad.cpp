#include "ad.h"
#include "snpin.h"

#include "dl.h"
#include "mf.h"
#include "ff.h"
#include "dand.h"
#include "dor.h"
#include "snproperty.h"

#include "string.h"


// TODO 1: check functionality
SnAD::SnAD(int id, const char* const name, const char * const props)
	:SnObject(id, name)
{
	alloc(2,4,0,1);
	DelaySrc = 0;

//	addInput(new ((SnPinBool*)get(sizeof(SnPinBool))) SnPinBool("S", 11));
	addInput(snaiksm(SnPin<bool>(11, &S)));
	addInput(snaiksm(SnPin<bool>(12, &R)));

	dl = snaiksm(SnDL(1));

	// see SnDAND for more explanation
	addOutput(snaiksm(SnPin<bool>(51, &dl->Q)));
	addOutput(snaiksm(SnPin<bool>(52, &dl->rising)));
	addOutput(snaiksm(SnPin<bool>(53, &dl->falling)));
	addOutput(snaiksm(SnPin<bool>(54, &dl->Qn)));

	addPropterty(snaiksm(SnProperty<snanalog_t>("Delay", &Delay)));

	Delay = 1; // default value
	counterTicks = 0;
	setProps(props);
	dl->In = 0;
	dl->update();
}

void SnAD::update()
{
	if(DelaySrc){ // WARNING: workaround to propertysetter
		Delay = *DelaySrc;
	}
	uint64_t delayTicks = Delay*(1.0/SnObject::GlobalSampleTime);

	if(S && (counterTicks < delayTicks)){ // second condition, to prevent overflow
		counterTicks++;
	}
	if(!S || R){
		counterTicks = 0;
	}

	if(counterTicks >= delayTicks){
		dl->In = true;
	}else{
		dl->In = false;
	}
	dl->update();
}

Sn::ObjType SnAD::type()
{
	return Sn::AD;
}
