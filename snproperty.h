#ifndef SNPROPERTY_H
#define SNPROPERTY_H


#include "snobject.h"
#include "snpropertyinterface.h"
#include "config.h"

#ifdef USE_PLACEMENT_NEW
#include "memorydispenser.h"
#endif

#include <string.h> // for strlen/strcopy
#include <stdio.h> // for sprintf()
#include <stdlib.h> // for atoi()
#include <ctype.h> // for tolower()
#include <stdlib.h> // for atof()



template <typename T>
class SnProperty : public SnPropertyInterface
{
public:

	// value must be initialized externally
	SnProperty(const char * const name, T* staticMemory)
	{
#ifdef USE_PLACEMENT_NEW
		this->name = (char*) MemoryDispenser::get(strlen(name)+1);
#else
		this->name = new char[strlen(name)+1];
#endif
		strcpy(this->name, name);

		if(staticMemory){
			pValue = staticMemory;
		}
	}

	// TODO 5: what if T is char*
	// What method will the compiler call?
//	SnProperty(const char* const name, const T value); // typed value
//	SnProperty(const char* const name, const char* const value); // value as variant

	// TODO 5: wozu wird type benötigt?
	// wenn er benötigt wird, müssen die konstruktoren typ-spezifisch implementiert werden,
	// wo dann diese member-variable entsprechend gesetzt wird.
	//Sn::SignalType type;

	// override:
	void setValueByVariant(const char* variant, bool* ok=0); // must be implemented for each type
	char* getValueAsVariant(); // must be implemented for each type

	static void extractProperty(const char* const props, const char* const propName, char **value);
	static void extractProperty(const char* const props, const char* const propName,
								snanalog_t* const value, snanalog_t defaultValue=0.0);
	static void extractProperty(const char* const props, const char* const propName,
								bool* const value, bool defaultValue=false);

	T* pValue;

private:
	static char TempStr[30];
};

// implementation of templated methods
// TODO 3: make this functions available without any type-specialisation!
	// returns pointer to a TempStr, holding the value.
	// return 0, if value not found
	template <typename T>
	void SnProperty<T>::extractProperty(const char * const props,
										const char * const propName, char ** value)
	{
		// find number of separators:
		int iPropStart = 0;
		int lenProps = strlen(props);
		int i=0;
		while(i<lenProps){
			// seek to end of propName
			iPropStart = i;
			while(props[i] != ';' && i<lenProps){
				i++;
			}
			i++; // skip semicolon
			int propLen = i-iPropStart-1;

			int iValueStart = i;
			// seek end of value:
			while(props[i] != ';' && i<lenProps){
				i++;
			}
			i++;
			int valueLen = i-iValueStart-1;

			if(valueLen && strncmp(props+iPropStart, propName, propLen) == 0){
				// property found

				strncpy(TempStr, props+iValueStart, valueLen);
				TempStr[valueLen]=0; // terminate string // TODO 3 check limits of TempStr

				// fix locale decimal separator (--> in KiCad comma and point are allowed)
				char dp='.';
				if(strtod("1,5",0) > 1){
					dp = ','; // locale strtod() uses a comma as decimal separator
				}
				for(int i=0; TempStr[i]; i++){
					if(TempStr[i] == '.' || TempStr[i] == ','){
						TempStr[i] = dp;
					}
				}
				*value = TempStr;
				return;
			}
		}
		value = 0;
	}

	template <typename T>
	void SnProperty<T>::extractProperty(const char * const props, const char * const propName,
									 snanalog_t * const value, snanalog_t defaultValue)
	{
		char* valueStr=0;
		extractProperty(props, propName, &valueStr);

		if(valueStr){
			*value = atof(valueStr);
		}else{
			*value = defaultValue;
		}
	}

	template <typename T>
	void SnProperty<T>::extractProperty(const char * const props, const char * const propName, bool * const value, bool defaultValue)
	{
		char* valueStr=0;
		extractProperty(props, propName, &valueStr);

		if(valueStr){
			for(int i = 0; valueStr[i]; i++){
			  valueStr[i] = tolower(valueStr[i]);
			}
			if(strcmp(valueStr, "true")==0){
				*value = true;
			}else if(strcmp(valueStr, "false")==0){
				*value = false;
			}else{
				*value = atoi(valueStr); // implicit cast to bool
			}
		}else{
			*value = defaultValue;
		}
	}




#endif // SNPROPERTY_H
