#include "snobject.h"

#ifndef SNAIKS_DL_H_
#define SNAIKS_DL_H_


// Digital Line
class SnDL:public SnObject
{
public:
	SnDL(int id, const char* const name=0);

	void update();
	Sn::ObjType type();

	bool In;

	bool Q;
	bool rising; // is true for one update-cycle, when out went from low to high
	bool falling; // is true for one update-cycle, when out went from high to low
	bool Qn; // inverted out
};



#endif
