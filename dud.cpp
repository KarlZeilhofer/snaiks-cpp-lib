#include "dud.h"
#include "snpin.h"

SnDUD::SnDUD(int id, const char * const name)
	:SnObject(id,name)
{
	alloc(1,1,0,0);

	addInput(snaiksm(SnPin<bool>(11, &In)));
	addOutput(snaiksm(SnPin<bool>(51, &Out)));
}

void SnDUD::update()
{
	Out = In;
}

Sn::ObjType SnDUD::type()
{
	return Sn::DUD;
}

bool SnDUD::isTransparent()
{
	return false;
}
