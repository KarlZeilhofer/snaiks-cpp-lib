#include "ain.h"

#include "snpin.h"
#include "snproperty.h"

SnAIN::SnAIN(int id, const char* const name, const char * const props)
	:SnObject(id, name)
{
	alloc(0,1,0,1);

	addOutput(snaiksm(SnPin<snanalog_t>(51, &Out)));

	addPropterty(snaiksm(SnProperty<snanalog_t>("InitValue", &InitValue)));

	InitValue = 0;
	setProps(props);
	Out = InitValue;
}

void SnAIN::update()
{
	SnObject::update(); // sample output-pin
		// (for the case an external value is given as source for our output pin)
}

Sn::ObjType SnAIN::type()
{
	return Sn::AIN;
}
