#include "snobject.h"

#ifndef SNAIKS_DAND_H_
#define SNAIKS_DAND_H_


class SnDL;

//	logic or with 2 inputs
class SnDAND:public SnObject
{
public:
	SnDAND(int id, const char* const name=0);
	void update();
	Sn::ObjType type();

	bool In1;
	bool In2;

protected:
	SnDL* dl;
};

#endif
