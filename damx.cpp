#include "damx.h"
#include "snpin.h"

SnDAMX::SnDAMX(int id, const char* const name)
	:SnObject(id, name)
{
	In0 = 0;
	In1 = 0;
	Select = false;

	alloc(3,1,0,0);

	addInput(snaiksm(SnPin<snanalog_t>(11, &In0)));
	addInput(snaiksm(SnPin<snanalog_t>(12, &In1)));
	addInput(snaiksm(SnPin<bool>(13, &Select)));

	addOutput(snaiksm(SnPin<snanalog_t>(51, &Out)));

	Out = 0;
}

void SnDAMX::update()
{
	if(Select){
		Out = In1;
	}else{
		Out = In0;
	}
}

Sn::ObjType SnDAMX::type()
{
	return Sn::DAMX;
}
