

#include "snobject.h"

#ifndef SNAIKS_MF_H_
#define SNAIKS_MF_H_

// synomnym for MFET (Monoflop with Edge Trigger)
#define SnMFET SnMF

class SnDL;

// Mono Flop
class SnMF:public SnObject
{
public:
	SnMF(int id, const char* const name=0, const char* const props=0);
	void update();
	Sn::ObjType type();
	void print();

	bool S; // set
	bool R; // reset

	SnDL* dl;

	snanalog_t Period; // number of cycles out remains high, after S went low.
private:
	uint64_t counterTicks;
};

#endif
