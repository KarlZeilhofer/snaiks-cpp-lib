#include "snpin.h"
#include "string.h"
#include "stdio.h"

int SnPinInterface::globalPinsCounter = 0;

SnPinInterface::SnPinInterface(uint8_t number)
	:number(number)
{
	globalPinsCounter++;
#ifndef SNAIKS_NAMELESS_PINS
	this->name = (char*)MemoryDispenser::get(strlen(name)+1);
	strcpy(this->name, name);
#endif
}

int SnPinInterface::print(char *destStr, size_t maxLen)
{
//	if(type() == Sn::ST_BOOL){
	bool* pb = (bool*)outputAddress();
	snanalog_t* pd = (snanalog_t*)outputAddress();
	if(*pb == 0 || *pb == 1){ // both
		return snprintf(destStr, maxLen, "%c/%ld", (*pb)+'0', (int32_t)(*pd * 1000)); // TODO 0: fix Quick and dirty workaournd
	}else{ // double only
		return snprintf(destStr, maxLen, "%ld", (int32_t)(*pd * 1000)); // TODO 0: fix Quick and dirty workaournd
	}
//	}else if(type() == Sn::ST_ANALOG){
//		snanalog_t* p = outputAddress();
//		return snprintf(destStr, maxLen, "%lf", *p);
//	}else{
//		return 0;
//	}
}



void SnPinInterface::sample()
{
	memcpy(outputAddress(), sourceAddress(), typeSize());
}

template <>
Sn::SignalType SnPin<bool>::type() const
{
	return Sn::ST_BOOL;
}
template <>
Sn::SignalType SnPin<snanalog_t>::type() const
{
	return Sn::ST_ANALOG;
}


// TODO 3: special implementations (for now, compile error,
// invalid cast to abstract class type 'SnPin<double>'
// on addInput(.pin.)
/*
template <>
int SnPin<snanalog_t>::print(char* destStr, size_t maxLen) const
{
	return snprintf(destStr, maxLen, "%lf", *output);
}

template <>
int SnPin<bool>::print(char* destStr, size_t maxLen) const
{
	return snprintf(destStr, maxLen, "%d", *output);
}
*/
