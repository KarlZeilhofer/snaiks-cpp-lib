#include "srl.h"
#include "snpin.h"
#include "snproperty.h"

SnSRL::SnSRL(int id, const char * const name, const char * const props)
	:SnObject(id, name)
{
	alloc(2, 1, 0, 1);

	addInput(snaiksm(SnPin<snanalog_t>(11, &In)));
	addInput(snaiksm(SnPin<bool>(12, &R)));

	addOutput(snaiksm(SnPin<snanalog_t>(51, &Out)));

	addPropterty(snaiksm(SnProperty<snanalog_t>("Slewrate", &Slewrate)));
	Slewrate = 1.0; // 1 unit per second is default

	setProps(props);
}

void SnSRL::update()
{
	if(R){
		Out = 0;
	}else{
		snanalog_t dMax = SnObject::GlobalSampleTime * Slewrate;
		if(In > (Out+dMax)){
			Out = Out + dMax;
		}else if(In < (Out-dMax)){
			Out = Out - dMax;
		}else{
			Out = In;
		}
	}
}

Sn::ObjType SnSRL::type()
{
	return Sn::SRL;
}
