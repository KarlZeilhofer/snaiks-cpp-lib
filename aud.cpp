#include "aud.h"
#include "snpin.h"

SnAUD::SnAUD(int id, const  char* const name)
    :SnObject(id,name)
{
	alloc(1,1,0,0);

	addInput(snaiksm(SnPin<snanalog_t>(11, &In)));
	addOutput(snaiksm(SnPin<snanalog_t>(51, &Out)));
}

void SnAUD::update()
{
	Out=In;
}

Sn::ObjType SnAUD::type()
{
	return Sn::AUD;
}

bool SnAUD::isTransparent()
{
	return false;
}
