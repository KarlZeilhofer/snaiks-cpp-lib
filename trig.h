#include "snobject.h"

#ifndef SNAIKS_TRIG_H_
#define SNAIKS_TRIG_H_


// Digital Line
class SnTRIG:public SnObject
{
public:
	SnTRIG(int id, const char* const name=0);

	void update();
	Sn::ObjType type();
	void print();

	void trigger(); // called by main programm

	bool Out;
private:
	bool triggered;
};

#endif
