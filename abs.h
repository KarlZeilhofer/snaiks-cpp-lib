#include "snobject.h"

#ifndef SIGNALNET_ABS_H_
#define SIGNALNET_ABS_H_


// Absolute Value of a scalar (SnABS)
class SnABS:public SnObject
{
public:
	SnABS(int id, const char* const name=0);
	void update();
	Sn::ObjType type();

	snanalog_t In;
	snanalog_t Out;
};

#endif
