#ifndef SnAOUT_H
#define SnAOUT_H

#include "snobject.h"

// Provides an Analog Signal Sink
// to be used in the main programm
// TODO 5: provide callback-function
class SnAOUT : public SnObject
{
public:
	SnAOUT(int id, const char* const name=0);
	Sn::ObjType type();

	snanalog_t In;
};

#endif // SnAIN_H
