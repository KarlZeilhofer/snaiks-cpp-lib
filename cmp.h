#include "snobject.h"

#ifndef SNAIKS_CMP_H_
#define SNAIKS_CMP_H_

class SnDL;

// Comparator
class SnCMP:public SnObject
{
public:
	SnCMP(int id, const char* const name=0);
	void update();
	Sn::ObjType type();
	void print();

	snanalog_t In1;
	snanalog_t In2;

	SnDL* dl;
};


#endif
