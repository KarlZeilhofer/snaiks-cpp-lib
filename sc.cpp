#include "sc.h"
#include "snpin.h"

SnSC::SnSC(int id, const char * const name)
	:SnObject(id, name)
{
	alloc(1,1,0,0);

	addInput(snaiksm(SnPin<snanalog_t>(11, &In)));

	addOutput(snaiksm(SnPin<snanalog_t>(51, &Out)));
}

void SnSC::update()
{
	Out = -In;
}

Sn::ObjType SnSC::type()
{
	return Sn::SC;
}
