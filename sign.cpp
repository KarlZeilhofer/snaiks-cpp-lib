#include "sign.h"
#include "snpin.h"

SnSIGN::SnSIGN(int id, const char * const name)
	:SnObject(id, name)
{
	alloc(1,1,0,0);

	addInput(snaiksm(SnPin<snanalog_t>(11, &In)));

	addOutput(snaiksm(SnPin<snanalog_t>(51, &Out)));
}

void SnSIGN::update()
{
	if(In > 0){
		Out = 1;
	}else if(In < 0){
		Out = -1;
	}else{
		Out = 0;
	}
}

Sn::ObjType SnSIGN::type()
{
	return Sn::SIGN;
}
