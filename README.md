# Snaiks - From Signals and Nets in KiCad to Embedded C++ Code



## Conventions

### KiCad

#### Symbol Creation:
* Symbol-Pins aro only of Type Input and Output
* Pin Numbers are visible
* Pin Names and Numbers are each unique within a symbol
* Input Pins start with 11
* Output Pins start with 51
* Pin Length = 100mil
* Inputs are left, outputs are right.


The Name and Default-Value for each symbol is the short class name:  
E.g.: class SnDAND{}  
* field reference = DAND?  
* component name = DAND  
* field value = DAND  

##### Value:
	the value of a component in KiCad-Schematic is a descriptive text, can contain any characters inkluding spaces

##### Signal Types
	are specified in C++ only (bool, snanalog_t)

##### Unconnected pins
	must be terminated with not-connected flag (X)

##### Hierarchical Sheets:
	Use PIN symbol in KiCad!  
		pin numbers are assigned explicitly by adding a PinDouble or PinBool symbol to each
		hierarchical pin in the subsheet.  
		The reference is for example PIN1305, which means this is Sheet-Pin Nr. 5 on sheet 13.
		In C++ this "parts" are processed separatly.
			1. they lead to pin-generation e.g.
				addInput(snaiksm(SnPin<snanalog_t>("Vref_1", 1)));
				in future version is will look like this:
				addInput(snaiksm(SnPin<snanalog_t>("Vref_1", 1)));
			2. on connects, this refers to this->getPin() e.g.
				Sn::connect(this->getPin(2), getObj(Sn::ABS, 2)->getPin(11));

	a subsheet must be implemented as a separate c++ class.
	in C++ it has pins, very analoug to a normal object.


The ID of sub-objects is the number without the "sheet-prefix"  
	e.g. if a MF is called MF217 on page 2 in KiCad, then it gets the id=17.
	the object itself must then have the id=2.


##### Convinient Constatnts:
* Logic high =	snaiks.lib:H = DIN with InitValue=1;
* Logic low =		snaiks.lib:L = DIN with InitValue = 0;
* Analog 0 =		snaiks.lib:GND = AIN with InitValue = 0;

##### Properties:
* Properties are specified in Components fields as strings
* Zero-Values:
		The C++ side default value is allways 0 or false (for snanalog_t and bool respectivly)
		zero-values are therefore not exported into C++ code!

##### (TODO, not working) PropertySetter-Class SnPS
		to change property values within the Snaiks-Schematic, a compontent called
		Property Setter (SnPS) was introduced.
		It must only change properties of SnObjects on the same sheet!

##### Implementation
eine Sub-Klasse von SnObject behandelt Properties typischerweise so.
derzeit sind keine anonymen properties vorgesehen.
Daher muss es zu jedem Prp. auch eine member-variable geben.

```
.h
	int Period;
.cpp constructor()
	addProp(new SnProperty<int>("Period", &Period));
	...
	SnObject::setProps(propertiesStr); // "Period;10;Gain;0.45;Offset;2.0"
```

### C++ Object Implementation:
Every object that is derived from SnObject must have one of these constructors:
1. e.g. `SnMF(int id, const char* const name=0, const char* const properties=0);`
2. e.g. `Controller(int id, const char* const name=0);`

For name and id the super-constructor must be called!
```
SnDAND(int id, const char* const name=0, const char* const properties=0)
	:SnObject(id,name) // call the super constructor
{
	// object specific code
}
```

## KiCad Netlist Structure:
```
design
	source
	date
	tool
	sheet[number]
		name
		tstamps
		title_block
			title
			company
			rev
			date
			source
			comment[1..4]
				number
				value

components
	comp[ref]
		value
		fields
			field[name]
				value
		libsource
			lib
			part
		sheetpath
			names
			tstamps
		tstamp

libparts
	libpart[lib,part]
		fields
			field[name]
				value
		pins
			pin[num,name,type]
				
libraries
	library[logical]
		uri

nets
	net[code]
		name
		node[ref,pin]
```

