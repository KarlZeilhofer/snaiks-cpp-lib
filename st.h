#include "snobject.h"

#ifndef SNAIKS_ST_H_
#define SNAIKS_ST_H_

class SnDL;

// Schmitt Trigger
class SnST:public SnObject
{
public:
public:
	SnST(int id, const char* const name=0, const char* const props=0);
	void update();
	Sn::ObjType type();


	snanalog_t In;

	SnDL* dl;

	snanalog_t UpperLimit;
	snanalog_t LowerLimit;
};

#ifdef SNAIKS_ENABLE_UNIT_TESTS

class SnST_UnitTest : public SnObject
{
public:
	SnST_UnitTest(int id, const char* const name=0);

	// SnObject interface
public:
	Sn::ObjType type(){
		return (Sn::ObjType)(Sn::ST + Sn::UNIT_TEST_BASE);
	}
};

#endif


#endif
