#include "snobject.h"

#ifndef SNAIKS_MUL_H_
#define SNAIKS_MUL_H_


// Multiply
class SnMUL:public SnObject
{
public:
	SnMUL(int id, const char* const name=0);
	void update();
	Sn::ObjType type();

	snanalog_t In1;
	snanalog_t In2;

	snanalog_t Out;
};


#endif
