/*
 * snaiks.cpp
 *
 *  Created on: May 22, 2014
 *      Author: Karl Zeilhofer
 *      karl@zeilhofer.co.at
 *
 */


/*
 *  This module is used to implement a periodically updated signal-network
 *  with digital and analog devices like
 *  	AnalogLine
 *  	DigitalLine
 *  	FlipFlop
 *  	SchmittTrigger
 *
 *  All signals are propagated to the output once in a cycle.
 *    by calling SNxx_update()
 *
 *  If a Null-Pointer is passed as an input-signal, local memory is allocated for its states,
 *    and can then be written from external.
 *
 */

#include "snobject.h"
#include <string.h>

#include "config.h"

#ifdef USE_PLACEMENT_NEW
#include "memorydispenser.h"
#endif

#include "snpin.h"
#include "snproperty.h"




#ifdef ARDUINO_DEBUG
#include <Arduino.h>
#endif


#ifdef USE_PLACEMENT_NEW
// Arduino (for AVR-processors) needs a custom definition of the placement new operator!
// please refer to http://arduino.land/FAQ/content/4/20/en/can-i-use-new-and-delete-with-arduino.html
void *operator new( size_t size, void *ptr ){
	(void) size; // remove unused warning
	return ptr;
}
#endif


snanalog_t SnObject::GlobalSampleTime = 1.0; // by default: 1 second // TODO 5: make a multi-rate system possible
int SnObject::globalObjectsCounter = 0;

// NOTE: when name is used in a read access, check for 0-pointer!
SnObject::SnObject(int id, const char* const name)
	:id(id)
{
	globalObjectsCounter++;
#ifndef SNAIKS_NAMELESS_OBJECTS
	this->name=0;
	if(name){
		this->name = (char*)MemoryDispenser::get(strlen(name)+1);
		strcpy(this->name, name);
	}else{
		this->name=0;
	}
#else
	(void) name; // remove unused warning
#endif
}


// by default, we call every inputs' and objects' sample()
// transparency is also taken into account.
void SnObject::sample()
{
	for(int i=0; i<inputs.size(); i++){
		inputs[i]->sample(); // pins are allways transparent
	}
	for(int i=0; i<objects.size(); i++){
		objects[i]->sample();

#ifndef SNAIKS_ALL_NONTRANSPARENT
		// if this object is transparent, call its update() immediatly
		if(objects[i]->isTransparent()){
			objects[i]->update();
		}
#endif
	}
}

// by default, we call every object's update() and every outputs' sample()
void SnObject::update()
{
	for(int i=0; i<objects.size(); i++){
		if(!objects[i]->isTransparent()){
			objects[i]->update(); // call update only for non-transparent objects
				// transparent object have been updated already in sample()
		}
	}

	// TODO 1: transparency: what about the output pins?
	// is there a difference between tranparent and non-transparent objects?
	for(int i=0; i<outputs.size(); i++){
		outputs[i]->sample();
	}
}

// By default a SnObject is transparent
// if not, this function must be overridden.
bool SnObject::isTransparent()
{
	return true;
}

void SnObject::print(char* destStr, size_t maxLen)
{
	// TODO 1: print name
	size_t i=0;
#ifndef SNAIKS_NAMELESS_OBJECTS
	i+= snprintf(destStr, maxLen, "%s: ", name);
#endif
	// TODO 1: print inputs
	for(int k=0; k<inputs.size(); k++){
		char value[20];
		inputs[k]->print(value, 19);
		i+=snprintf(destStr+i, maxLen-i, "in%d=%s; ", inputs[k]->number, value);
	}
	for(int k=0; k<outputs.size(); k++){
		char value[20];
		outputs[k]->print(value, 19);
		i+=snprintf(destStr+i, maxLen-i, "out%d=%s; ", outputs[k]->number, value);
	}
}

void SnObject::alloc(int nInputs, int nOutputs, int nObjects, int nProperties)
{
	inputs.allocate(nInputs);
	outputs.allocate(nOutputs);
	objects.allocate(nObjects);
	properties.allocate(nProperties);
}

void SnObject::addObj(SnObject *obj)
{
	if(obj){
#ifdef ARDUINO_DEBUG
		Serial.print("Adding SnObject Nr ");
		Serial.print(obj->globalObjectsCounter);
		Serial.print(", heapsize=");
		Serial.println(MemoryDispenser::dispensedSize());
#endif
		// check, if we have such an object allready in our list
		SnObject* h = getObj(obj->type(), obj->id, false);
		if(h == 0){
			objects.add(obj);
		}else{
			SnaiksErrorCallback("duplicate obj-id", __FILE__, __LINE__);
		}
	}
}

// typical call could look like this:
//		addPin(&SnPinDouble("In1", 11, &In1));
void SnObject::addPin(SnPinInterface *pin, Sn::IOType io)
{
	if(pin){
#ifdef ARDUINO_DEBUG
		Serial.print("Adding SnPin Nr ");
		Serial.print(pin->globalPinsCounter);
		Serial.print(", heapsize=");
		Serial.println(MemoryDispenser::dispensedSize());
#endif

		// auto assign pin number, if pin number is zero
		if(pin->number == 0){
			if(io == Sn::SN_INPUT){
				pin->number = 1011+inputs.size();
			}
			if(io == Sn::SN_OUTPUT){
				pin->number = 1051+outputs.size();
			}
		}


		SnContainer<SnPinInterface*>* c;
		if(io == Sn::SN_INPUT){
			c = &inputs;
		}else{
			c = &outputs;
		}
		// check, if we have such a pin allready in our list
		SnPinInterface* h1 = 0;
#ifndef SNAIKS_NAMELESS_PINS
		h1 = getPin(pin->name);
#endif
		SnPinInterface* h2 = getPin(pin->number, false);
		if(h1 == 0 && h2 == 0){
			c->add(pin);
		}else{
			SnaiksErrorCallback("duplicate pin-name or number", __FILE__, __LINE__);
		}
	}
}

void SnObject::addPropterty(SnPropertyInterface *prop)
{
	if(prop){
#ifdef ARDUINO_DEBUG
		Serial.print("Adding SnProperty Nr");
		Serial.print(prop->globalCounter);
		Serial.print(", heapsize=");
		Serial.println(MemoryDispenser::dispensedSize());
#endif
		properties.add(prop);
	}
}


SnObject *SnObject::getObj(Sn::ObjType t, int id, bool throwError)
{
	for(int i=0; i<objects.size(); i++){
		if(objects[i]->type() == t && objects[i]->id == id){
			return objects[i];
		}
	}
	if(throwError){
		char str[150];
		sprintf(str,
		   "subobject not found "
		   "(this.type=%d, this.id=%d ,sub-type=%d, sub-id=%d)",
		   type(), this->id, t, id);
		SnaiksErrorCallback(str, __FILE__, __LINE__);
	}
	return 0;
}

SnPinInterface *SnObject::getPin(int pinNumber, bool throwError)
{
	for(int i=0; i<inputs.size(); i++){
		if(inputs[i]->number == pinNumber){
			return inputs[i];
		}
	}
	for(int i=0; i<outputs.size(); i++){
		if(outputs[i]->number == pinNumber){
			return outputs[i];
		}
	}
	if(throwError){
		char str[150];
		sprintf(str, "pin not found (objtype=%d,objid=%d,pinNr=%d)", type(), id, pinNumber);
		SnaiksErrorCallback(str, __FILE__, __LINE__);
	}
	return 0;
}

#ifndef SNAIKS_NAMELESS_PINS
SnPin *SnObject::getPin(const char* const pinName)
{
	for(int i=0; i<inputs.size(); i++){
		if(strcmp(inputs[i]->name, pinName) == 0){
			return inputs[i];
		}
	}
	for(int i=0; i<outputs.size(); i++){
		if(strcmp(outputs[i]->name, pinName) == 0){
			return outputs[i];
		}
	}
	return 0; // not found in list
}
#endif

/// Parse Properties-Sting and fill our properties in the properties-container
void SnObject::setProps(const char * const props)
{
	if(props){
		for(int i=0; i<properties.size(); i++){
			char* value;
			// TODO 3: extractProperty without type!
			// INFO: bool is used here just as anytype
			SnProperty<bool>::extractProperty(props, properties[i]->getName(), &value);
			properties[i]->setValueByVariant(value);
		}
	}
}

Sn::ObjType SnObject::type()
{
	return (Sn::UNDEFINED_OBJTYPE);
}

void Sn::connect(SnPinInterface *from, SnPinInterface *to)
{
	if(from && to){
		if(from->type() != to->type()){
			(*SnaiksErrorCallback)("connect type mismatch", __FILE__, __LINE__);
		}

		to->setSource(from->outputAddress());
	}else{
		if(!from)
			(*SnaiksErrorCallback)("connect from-object nullpointer", __FILE__, __LINE__);
		if(!to)
			(*SnaiksErrorCallback)("connect to-object nullpointer", __FILE__, __LINE__);
	}
}
