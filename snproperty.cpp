#include "snproperty.h"
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

// type specific implementations: TODO 5: many more types
// implemented:
/* snanalog_t
 * int32_t
 * bool
 * char*
 */


template <class T>
char SnProperty<T>::TempStr[30] = {0};
int SnPropertyInterface::globalCounter = 0;




//	// char*
//		template <>
//		char *SnProperty<char*>::getValueAsVariant()
//		{
//			sprintf(TempStr, "%s", *pValue);
//		}

//		template <>
//		void SnProperty<char*>::setValueByVariant(char* variant, bool *ok)
//		{
//			strcpy(*pValue, variant) // TODO 5: error checking with custom parsing function
//			*ok=true;
//		}

// Type-Specific Implementations:
// bool}
	template <>
	char *SnProperty<bool>::getValueAsVariant()
	{
		sprintf(TempStr, "%d", *pValue);
		return TempStr;
	}

	template <>
	void SnProperty<bool>::setValueByVariant(const char* variant, bool *ok)
	{
		bool v;
		if(strncmp(variant, "true",4)==0){
			v=true;
		}else if(strncmp(variant, "false",5)==0){
			v=false;
		}else{
			v = atoi(variant); // for 0 or 1
		}
		if(ok){
			*ok = true; // TODO 5: Error handling
		}
		*pValue = v;
	}

// snanalog_t
	template <>
	char *SnProperty<snanalog_t>::getValueAsVariant()
	{
		sprintf(TempStr, "%lf", *pValue);
		return TempStr;
	}


	template <>
	void SnProperty<snanalog_t>::setValueByVariant(const char* variant, bool *ok)
	{
		snanalog_t v = strtod(variant,0); // TODO 0: error checking with custom parsing function
		// TODO 0: fix bug. returns 70 for "70.5"
		if(ok){
			*ok=true;
		}
		*pValue = v;
	}

// int32_t
	template <>
	char *SnProperty<int32_t>::getValueAsVariant()
	{
		sprintf(TempStr, "%d", *pValue);
		return TempStr;
	}

	template <>
	void SnProperty<int32_t>::setValueByVariant(const char* variant, bool *ok)
	{
		int32_t v = atol(variant); // TODO 5: error checking with custom parsing function
		if(ok){
			*ok=true;
		}

		*pValue = v;
	}

