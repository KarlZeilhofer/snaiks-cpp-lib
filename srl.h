#ifndef SNSRL_H
#define SNSRL_H

#include "snobject.h"

// Slew Rate Limiter
class SnSRL : public SnObject
{
public:
	SnSRL(int id, const char* const name=0, const char* const props=0);
	void update();
	Sn::ObjType type();

	snanalog_t In;
	bool R; // reset output to zero

	snanalog_t Out;

	// Property
	snanalog_t Slewrate; // in units per second
};

#endif // SNSRL_H
