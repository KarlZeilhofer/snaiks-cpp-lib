#include "sat.h"
#include "snpin.h"
#include "snproperty.h"

SnSAT::SnSAT(int id, const char * const name, const char* const props)
	:SnObject(id, name)
{
	alloc(1,1,0,2);
	addInput(snaiksm(SnPin<snanalog_t>(11, &In)));
	addOutput(snaiksm(SnPin<snanalog_t>(51, &Out)));

	addPropterty(snaiksm(SnProperty<snanalog_t>("High", &High)));
	addPropterty(snaiksm(SnProperty<snanalog_t>("Low", &Low)));

	// default values
	High = 1.0;
	Low = 0.0;

	setProps(props);
}

void SnSAT::update()
{
	if(In >= High){
		Out = High;
	}else if(In <= Low){
		Out = Low;
	}else{
		Out = In;
	}
}

Sn::ObjType SnSAT::type()
{
	return Sn::SAT;
}
