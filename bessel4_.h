#ifndef SNBESSEL4__H
#define SNBESSEL4__H

#include "snobject.h"

class SnBESSEL4_: public SnObject
{
public:
	SnBESSEL4_(int id, const char* const name=0, const char * const props=0);
	void update();
	Sn::ObjType type();

	snanalog_t In;
	bool R;

	snanalog_t Out;
private:
	snanalog_t UpdateRateDivider; // TODO 0: change back to int32, if possible
	uint32_t updateCounter;

	//Low pass bessel filter order=4 alpha1=0.025
	// http://www.schwietering.com/jayduino/filtuino/index.php?characteristic=be&passmode=lp&order=4&usesr=usesr&sr=1000&frequencyLow=125&noteLow=&noteHigh=&pw=pw&calctype=double&run=Send
	class  FilterBeLp4
	{
	public:
		void reset(){
			for(int i=0; i <= 4; i++)
				v[i]=0.0;
		}
	public:
		FilterBeLp4()
		{
			for(int i=0; i <= 4; i++)
				v[i]=0.0;
		}
	private:
		double v[5];
	public:
		double step(double x) //class II
		{
			v[0] = v[1];
			v[1] = v[2];
			v[2] = v[3];
			v[3] = v[4];
			v[4] = (1.400624033505888655e-4 * x)
				 + (-0.47549554437894708814 * v[0])
				 + (2.26718854925745905149 * v[1])
				 + (-4.08003499939867797508 * v[2])
				 + (3.28610099606655658988 * v[3]);
			return
				 (v[0] + v[4])
				+4 * (v[1] + v[3])
				+6 * v[2];
		}
	};

	FilterBeLp4 f;
};

#endif // SNBESSEL4__H
