#ifndef SNCONTAINER_H
#define SNCONTAINER_H

#include "config.h"

#ifdef USE_PLACEMENT_NEW
#include "memorydispenser.h"
#endif

#include "snerrorcallback.h"

// used for SnObjects, SnPins and SnProperties
template <class T>
class SnContainer{
public:
	static int globalCounter;
private:
	T* elems;
	int allocSize; // total size

public:
	SnContainer(){
		allocSize=0;
		nElems=0;
		elems=0;
	}

	void allocate(int nElements){
		allocSize = nElements;
		if(nElements){
#ifdef USE_PLACEMENT_NEW
			elems = (T*) MemoryDispenser::get(sizeof(T)*nElements);
#else
			elems = new T[nElements];
#endif
			if(!elems){
				(*SnaiksErrorCallback)("elems-nullpointer", __FILE__, __LINE__);
			}
		}else{
			elems = 0; // very important to set null-pointer here!
		}
	}

	void allocate(int nElements, T* staticArray)
	{
		allocSize = nElements;
		elems = staticArray;
	}


	T operator[](int i){
		if(elems && (i < allocSize)){
			return elems[i];
		}else{
			(*SnaiksErrorCallback)("out of bounds", __FILE__, __LINE__);
			return elems[0];
		}
	}

	void add(T& p){
		if(elems){
			if((size() < allocSize)){
				elems[size()] = p;
				nElems++;
			}else{
				(*SnaiksErrorCallback)("container overrun", __FILE__, __LINE__);
			}
		}else{
			(*SnaiksErrorCallback)("elems-nullpointer", __FILE__, __LINE__);
		}
	}

private:
	int nElems; // used elements (see add())

public:
	int size(){return nElems;}
};

template <class T>
int SnContainer<T>::globalCounter = 0;


#endif // SNCONTAINER_H
