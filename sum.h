#include "snobject.h"

#ifndef SNAIKS_SUM_H_
#define SNAIKS_SUM_H_


// Sum
class SnSUM:public SnObject
{
public:
	SnSUM(int id, const char* const name=0);
	void update();
	Sn::ObjType type();

	snanalog_t In1;
	snanalog_t In2;

	snanalog_t Out;
};

#endif
