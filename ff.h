#include "snobject.h"

#ifndef SNAIKS_FF_H_
#define SNAIKS_FF_H_

class SnDL;


// Flip Flop
class SnFF:public SnObject
{
public:
	SnFF(int id, const char* const name=0);

	void update();
	Sn::ObjType type();
	void print();

	bool S; // set input
	bool R; // reset input
	SnDL* dl;
};

#endif
