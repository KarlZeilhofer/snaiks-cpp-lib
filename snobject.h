/*
 * snobject.h
 *
 *  Created on: May 22, 2014
 *      Author: Karl Zeilhofer
 *      eMail: karl@zeilhofer.co.at
 *
 */

#ifndef SNOBJECT_H_
#define SNOBJECT_H_



// TODO 5: SnContainer::allocate(int n) ersetzten durch
// SnContainer::open(void* tempMemory, int bytesSize);
// SnContainer::close();
// vor den Aufrufen von SnContainer::add(), muss der container geöffnet werden.
// und danach wieder geschlossen werden.
// beim schließen ist dann die anzahl der elemente bekannt, und es kann optimale speichergröße allokiert werden.
// dies setzt jedoch einen ausreichend großen temporären speicher voraus.
// Problem: beim typischen rekursiven aufruf der konstruktoren braucht man dann N mal die maximale speichergröße,
// wobei N hier die rekursionstiefe ist.

/*
 * List of implemented objects
 *
 * Analog Inputs:
 *	Absolute		(SnABS)
 *	AnalogLine		(SnAL)
 *	Comparator		(SnCMP)
 *	AnalogMux2		(SnDAMX)
 *	Difference		(SnDIF)
 *	Divide			(SnDIV)
 *	Hold			(SnHLD)
 *  Integrator		(SnINT)
 *	Maximum			(SnMAX)
 *  Minimum			(SnMIN)
 *  Multiply		(SnMUL)
 *  SignChanger		(SnSC)
 *  Signum			(SnSIGN)
 *  Sum				(SnSUM)
 *	SchmittTrigger	(SnST)
 *
 * Digital Inputs:
 *	ActivationDelay	(SnAD)
 *	And2			(SnDAND)
 *	DigitalLine		(SnDL)
 *	Or2				(SnDOR)
 *	DigitalMux2		(SnDDMX)
 *	FlipFlop		(SnFF)
 *	MonoFlop		(SnMF)
 *	And4			(SnQAND)
 *	Or4				(SnQOR)
 *	Timer			(SnTIMER)
 *
 */

/*
 * An Objects consists of one or more inputs,
 * and/or one or more outputs and at least an update function.
 *
 * Optionally it contains a list of sub-objects.
 *
 * Optionally it contains a list of properties.
 *
 */

// NOTE: AVR-GCC implements double as 32-bit float, so keep the resolution in mind!
typedef double snanalog_t;

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include "sncontainer.h"
#include "config.h"

#ifdef USE_PLACEMENT_NEW
#include "memorydispenser.h"
#endif





// snaiksm...snaiks malloc
#ifdef USE_PLACEMENT_NEW
#define snaiksm(x) new (MemoryDispenser::get(sizeof(x))) x

// Arduino (for AVR-processors) needs a custom definition of the placement new operator!
// please refer to http://arduino.land/FAQ/content/4/20/en/can-i-use-new-and-delete-with-arduino.html
void *operator new( size_t size, void *ptr );

#else
#define snaiksm(x) (new x)
#endif


// forward declarations:
class SnPinInterface;
class SnPropertyInterface;


class Sn
{
public:
	static const int MAX_NAME_LEN = 30;

	// TODO 4: provide strings for all object types
	enum ObjType{
		UNDEFINED_OBJTYPE = 0,
		// analog types:
		ABS=1001, AIN, AMX, AL, AOUT, AUD, BESSEL4_, CMP, DAMX, DIF, DIV, HLD, INT, MAX, MIN, MUL, SAT, SC, SIGN, SRL, SUM, ST,
		// digital types:
		AD=2001, DAND, DIN, DMX, DL, DOUT, DOR, DDMX, DUD, FF, MF, QAND, QOR, TIMER, TRIG,
		// custom-type:
		CUSTOM_OBJECT = 2501,
		// special types:
		PS=2601, // Property Setter (TODO)
		PIN,// NOTE: no implementaition, so we get a compile-error, if it apperas in the code
		UNIT_TEST_BASE=3000
	};

	enum SignalType{
		UNDEFINED_SIGTYPE = 10000, ST_BOOL=10001, ST_ANALOG
	};

	// NOTE, not used for now
	enum IOType{
		SN_INPUT = 11001, SN_OUTPUT
	};

	static void connect(SnPinInterface* from, SnPinInterface* to);
};


class SnObject
{
public:
	// TODO 3: erweitern auf Konstruktor mit Parent
	SnObject(int id, char const* name=0);
	static int globalObjectsCounter;

	// common functions:
	void sample();
	virtual void update();
	virtual Sn::ObjType type()=0;
	bool isTransparent();
	void print(char *destStr, size_t maxLen);

	void alloc(int nInputs, int nOutputs, int nObjects, int nProperties);
	void addObj(SnObject* obj);
	void addPin(SnPinInterface* pin, Sn::IOType io);
	void addInput(SnPinInterface* pin){addPin(pin, Sn::SN_INPUT);} // convinient function
	void addOutput(SnPinInterface* pin){addPin(pin, Sn::SN_OUTPUT);} // convinient function
	void addPropterty(SnPropertyInterface* prop);

	SnObject* getObj(Sn::ObjType t, int id, bool throwError=true);


	SnPinInterface* getPin(int pinNumber, bool throwError=true);
#ifndef SNAIKS_NAMELESS_PINS
	SnPin* getPin(const char* const pinName);
#endif
	void setProps(const char* const props);

	// TODO 5, perhaps it's more efficient to allocate containers if needed,
	// since objects and properties are mostly empty
	SnContainer<SnPinInterface*> inputs;
	SnContainer<SnPinInterface*> outputs;
	SnContainer<SnObject*> objects;
	SnContainer<SnPropertyInterface*> properties;

#ifndef SNAIKS_NAMELESS_OBJECTS
	char* name; // this is optional, and corresponds to KiCad's value
#endif



	// e.g. object AND201 splitts up into
	//   type=Sn::AND and id=1, sheet=2 is not stored here
	int id;

	static snanalog_t GlobalSampleTime;
};





#endif /* SNOBJECT_H_ */















