#include "snobject.h"

#ifndef SNAIKS_DAMX_H_
#define SNAIKS_DAMX_H_


// Dual Analog Mux
class SnDAMX:public SnObject
{
public:
	SnDAMX(int id, const char* const name=0);
	void update();
	Sn::ObjType type();

	// Inputs:
	snanalog_t In0;
	snanalog_t In1;
	bool Select;

	// Output:
	snanalog_t Out;
};


#endif
