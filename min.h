#include "max.h"

#ifndef SNAIKS_MIN_H_
#define SNAIKS_MIN_H_


// 
class SnMIN:public SnMAX
{
public:
	SnMIN(int id, const char* const name=0);
	void update();
	Sn::ObjType type();
};

#endif
