#include "qand.h"

#ifndef SNAIKS_QOR_H_
#define SNAIKS_QOR_H_

//	logic or with 4 inputs
class SnQOR:public SnQAND
{
public:
	SnQOR(int id, const char* const name=0);
	void update(); // override
	Sn::ObjType type();
};

#endif
