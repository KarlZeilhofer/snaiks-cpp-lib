#ifndef SNCB_H
#define SNCB_H

#include "snobject.h"

// custom block
// use this as a base-class for custob block implementations
class SnCB : public SnObject
{
public:
	SnCB(int id, const char* const name);
};

#endif // SNCB_H
