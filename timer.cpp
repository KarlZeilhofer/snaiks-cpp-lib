#include "timer.h"
#include "snpin.h"

SnTIMER::SnTIMER(int id, const char * const name)
	:SnObject(id, name)
{
	En = false;
	R = false;

	alloc(2, 1, 0, 0);

	addInput(snaiksm(SnPin<bool>(11, &En)));
	addInput(snaiksm(SnPin<bool>(12, &R)));

	addOutput(snaiksm(SnPin<snanalog_t>(51, &Out)));

	counterTicks=0;
}

void SnTIMER::update()
{
	if(En){
		counterTicks++;
		Out = counterTicks*GlobalSampleTime;
	}

	if(R){
		counterTicks=0;
		Out=0;
	}
}

Sn::ObjType SnTIMER::type()
{
	return Sn::TIMER;
}
