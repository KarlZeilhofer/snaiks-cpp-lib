#include "snobject.h"

#ifndef SNAIKS_DDMX_H_
#define SNAIKS_DDMX_H_

// Dual Digital Mux
class SnDDMX:public SnObject
{
public:
	SnDDMX(int id, const char* const name=0);

	void update();
	Sn::ObjType type();


	// Inputs:
	bool In0;
	bool In1;
	bool Select;

	// Output:
	bool Out;
};



#endif
