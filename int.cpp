#include "int.h"
#include "snpin.h"


SnINT::SnINT(int id, const char * const name)
	:SnObject(id, name)
{
	alloc(2,1,0,0);

	addInput(snaiksm(SnPin<snanalog_t>(11, &In)));
	addInput(snaiksm(SnPin<bool>(12, &R)));

	addOutput(snaiksm(SnPin<snanalog_t>(51, &Out)));
}



/* Testvecotrs, for GlabolSampleTime = 1
 * 11,1,2,3,4,5,6,7
 * 12,1,1,0,0,0,1,1
 * 51,0,0,0,3,7,0,0
 */
void SnINT::update()
{
	if(!R){
		Out += lastIn*GlobalSampleTime; // TODO 2: precision for AVR-GCC
		lastIn = In;
	}else{ // reset is transparent
		Out=0;
		lastIn = 0;
	}
}

Sn::ObjType SnINT::type()
{
	return Sn::INT;
}
