#include "ps.h"
#include "snpin.h"
#include <string.h>

SnPS::SnPS(int id, const char * const name, const char * const props)
	:SnObject(id,name)
{
	// TODO 4: proper implementation
	alloc(1,0,0,0);
	addInput(snaiksm(SnPin<snanalog_t>(11, &In)));
}

SnPS::SnPS(int id, const char * const name,
		   SnObject *remoteObj, const char * const remotePropertyName)
	:SnObject(id, name), remoteObj(remoteObj)
{
	// TODO 5: check for nullpointers
	alloc(1,0,0,0);

	addInput(snaiksm(SnPin<snanalog_t>(11, &In)));

	strcpy(remoteProps, remotePropertyName);
	strcat(remoteProps, ";");
}

void SnPS::update()
{
	// TODO 4: Implement Property Setter update()!
}

Sn::ObjType SnPS::type()
{
	return Sn::PS;
}
