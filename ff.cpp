#include "ff.h"
#include "dl.h"
#include "snpin.h"


SnFF::SnFF(int id, const char* const name)
	:SnObject(id, name)
{
	S = false;
	R = false;

	alloc(2,4,0,0);

	addInput(snaiksm(SnPin<bool>(11, &S)));
	addInput(snaiksm(SnPin<bool>(12, &R)));

	dl = snaiksm(SnDL(1));

	// see SnDAND for more explanation
	addOutput(snaiksm(SnPin<bool>(51, &dl->Q)));
	addOutput(snaiksm(SnPin<bool>(52, &dl->rising)));
	addOutput(snaiksm(SnPin<bool>(53, &dl->falling)));
	addOutput(snaiksm(SnPin<bool>(54, &dl->Qn)));
}


// set when S is true
// reset when R is true
// leave Q unchanged, when S and R are false
void SnFF::update()
{
	if(S)
	{
		dl->In = true;
	}
	if(R) // reset dominates set
	{
		dl->In = false;
	}

	dl->update();
}

Sn::ObjType SnFF::type()
{
	return Sn::FF;
}


