#include "snobject.h"

#ifndef SNAIKS_DIN_H_
#define SNAIKS_DIN_H_


// Digital Line
class SnDIN:public SnObject
{
public:
	SnDIN(int id, const char* const name=0, const char* const props=0);

	void update();
	Sn::ObjType type();
	void print();

	bool Out;
	bool InitValue;
};



#endif
