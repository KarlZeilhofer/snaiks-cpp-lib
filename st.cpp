#include "st.h"
#include "dl.h"
#include "snpin.h"
#include "snproperty.h"

// Q ist high, when in >= UpperLimit and low when In <= LowerLimit
SnST::SnST(int id, const char * const name, const char * const props)
	:SnObject(id, name)
{
	In = 0;

	alloc(1, 4, 0, 2);

	addInput(snaiksm(SnPin<snanalog_t>(11, &In)));

	dl = snaiksm(SnDL(1));

	// see SnDAND for more explanation
	addOutput(snaiksm(SnPin<bool>(51, &dl->Q)));
	addOutput(snaiksm(SnPin<bool>(52, &dl->rising)));
	addOutput(snaiksm(SnPin<bool>(53, &dl->falling)));
	addOutput(snaiksm(SnPin<bool>(54, &dl->Qn)));

	addPropterty(snaiksm(SnProperty<snanalog_t>("UpperLimit", &UpperLimit)));
	addPropterty(snaiksm(SnProperty<snanalog_t>("LowerLimit", &LowerLimit)));

	UpperLimit = 1;
	LowerLimit = 0;
	setProps(props);

	if(LowerLimit > UpperLimit){
		SnaiksErrorCallback("invalid limits in SnST", __FILE__, __LINE__);
	}

	In = LowerLimit; // In must be below UpperLimit, to keep Q low

	// TODO 1: implement deconstructor (for unit-testing)
}

void SnST::update()
{
	if(In >= UpperLimit)
	{
		dl->In = true;
	}else if(In <= LowerLimit)
	{
		dl->In = false;
	}

	dl->update();
}

Sn::ObjType SnST::type()
{
	return Sn::ST;
}







#ifdef SNAIKS_ENABLE_UNIT_TESTS

#include "ain.h"

SnST_UnitTest::SnST_UnitTest(int id, const char* const name)
	:SnObject(id, name)
{
	inputs.allocate(0);
	outputs.allocate(0);
	SnObject* objectsArray[5]; // stack variable!!
	objects.allocate(5,objectsArray);
	properties.allocate(0);
	// object without props

	SnAIN ain(1, "AIN");
	SnST dut(1,"SchmittTrigger1", "LowerLimit;30.5;UpperLimit;70.5");
	addObj(&ain);
	addObj(&dut);

	snanalog_t in = 0;
	bool* Q		 = (bool*) getObj(Sn::ST, 1)->getPin(51)->outputAddress();
	bool* rise	 = (bool*) getObj(Sn::ST, 1)->getPin(52)->outputAddress();
	bool* fall	 = (bool*) getObj(Sn::ST, 1)->getPin(53)->outputAddress();
	bool* Qn	 = (bool*) getObj(Sn::ST, 1)->getPin(54)->outputAddress();

	Sn::connect(ain.getPin(51), dut.getPin(11));
	ain.getPin(51)->setSource(&in);

	// test-vector table:
	snanalog_t vIn[] = { 0, 1,29,30,31,70.4,70.5,71,30.6,30.5,71,  -1,-123};
	bool vQ[]		 = { 0, 0, 0, 0, 0,   0,   1, 1,   1,   0, 1,   0,   0};
	bool vR[]		 = { 0, 0, 0, 0, 0,   0,   1, 0,   0,   0, 1,   0,   0};
	bool vF[]		 = { 0, 0, 0, 0, 0,   0,   0, 0,   0,   1, 0,   1,   1};
	bool vQn[]		 = { 1, 1, 1, 1, 1,   1,   0, 0,   0,   1, 0,   1,   1};

	for(int i=0; vIn[i] != -123; i++){
		in = vIn[i];
		sample();
		update();


		char str[50];
		sprintf(str, "Unit-Test-Error: i=%d", i);
		if(vQ[i] != (*Q)){
			SnaiksErrorCallback(str, __FILE__, __LINE__);
		}
		if(vR[i] != (*rise)){
			SnaiksErrorCallback(str, __FILE__, __LINE__);
		}
		if(vF[i] != (*fall)){
			SnaiksErrorCallback(str, __FILE__, __LINE__);
		}
		if(vQn[i] != (*Qn)){
			SnaiksErrorCallback(str, __FILE__, __LINE__);
		}
	}
}

#endif // SNAIKS_ENABLE_UNIT_TESTS
