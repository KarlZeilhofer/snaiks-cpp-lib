#include "snobject.h"

#ifndef SNAIKS_TIMER_H_
#define SNAIKS_TIMER_H_


// Timer
// provides a pausable timer with internal 64-bit resolution
class SnTIMER:public SnObject
{
public:
	SnTIMER(int id, const char* const name=0);

	void update();
	Sn::ObjType type();


	bool R;
	bool En;

	bool enableLocal;

	snanalog_t Out; // time variable, incremented by 1 each update
private:
	uint64_t counterTicks;
};

#endif
