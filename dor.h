#include "dand.h"

#ifndef SNAIKS_DOR_H_
#define SNAIKS_DOR_H_


class SnDL;

//	logic or with 2 inputs
class SnDOR:public SnDAND
{
public:
	SnDOR(int id, const char* const name=0);
	void update();
	Sn::ObjType type();
};

#endif
