#include "snobject.h"

#ifndef SNAIKS_SC_H_
#define SNAIKS_SC_H_


// SignChanger: *(-1)
class SnSC:public SnObject
{
public:
	SnSC(int id, const char* const name=0);
	void update();
	Sn::ObjType type();


	snanalog_t In;

	snanalog_t Out;
};


#endif
