
#include "dl.h"
#include "snpin.h"


SnDL::SnDL(int id, const char* const name)
	:SnObject(id, name)
{

	alloc(1,4,0,0);

	addInput(snaiksm(SnPin<bool>(11, &In)));

	// see SnDAND for more explanation
	addOutput(snaiksm(SnPin<bool>(51, &Q)));
	addOutput(snaiksm(SnPin<bool>(52, &rising)));
	addOutput(snaiksm(SnPin<bool>(53, &falling)));
	addOutput(snaiksm(SnPin<bool>(54, &Qn)));

	Q = false;
	rising = false;
	falling = false;
	Qn = !Q;
}

void SnDL::update()
{
	bool oldOut = Q;

	Q = In;
	Qn = !Q;

	rising = false; // reset rising
	falling = false; // reset falling
	if(Q == true && oldOut == false)
	{
		rising = true; // set rising
	}
	if(Q == false && oldOut == true)
	{
		falling = true; // set falling
	}
}

Sn::ObjType SnDL::type()
{
	return Sn::DL;
}
