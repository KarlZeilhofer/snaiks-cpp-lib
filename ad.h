#include "snobject.h"

#ifndef SNAIKS_AD_H_
#define SNAIKS_AD_H_

class SnDL;

// Activation Delay
// with internal 64-bit resolution
class SnAD:public SnObject
{
public:
	SnAD(int id, const  char* const name, const  char* const props=0);
	void update();
	Sn::ObjType type();

public:
	bool S,R;

	SnDL* dl;

	// prperties:
	snanalog_t Delay;

	snanalog_t* DelaySrc; // NOTE: Workaround to porperty-setter
		// DelaySrc can be set in runtime to a foreign data-provider
//private: // TODO 0: make private again
	uint64_t counterTicks;
};



#endif
