#include "min.h"



SnMIN::SnMIN(int id, const char * const name)
	:SnMAX(id, name)
{
}

void SnMIN::update()
{
	if(In1 < In2){
		Out = In1;
	}else{
		Out = In2;
	}
}

Sn::ObjType SnMIN::type()
{
	return Sn::MIN;
}
