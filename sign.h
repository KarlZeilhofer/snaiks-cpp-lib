#include "snobject.h"

#ifndef SNAIKS_SIGN_H_
#define SNAIKS_SIGN_H_


// 
class SnSIGN:public SnObject
{
public:
	SnSIGN(int id, const char* const name=0);
	void update();
	Sn::ObjType type();

	snanalog_t In;

	snanalog_t Out;
};

#endif
