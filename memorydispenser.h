#ifndef MEMORYDISPENSER_H
#define MEMORYDISPENSER_H

#include <stddef.h> // for size_t and ptrdiff_t
#include <stdint.h>



// regarding static members in template classes refer to
// http://www.geeksforgeeks.org/templates-and-static-variables-in-c/



/**
 * @brief The MemoryDispenser class, a static class to get static memory
 *
 * this class has no free, since no memory should dynamically allocated and freed
 * in an embedded system, which is permanently on.
 *
 * TODO 5: implement a deallocateLastBlock for temporary memory needs
 */
class MemoryDispenser
{
public:
	/**
	  * @brief initialize pointers to allocated memory
	  */
	static void init(uint8_t* staticMemory, size_t size);

    /**
     * @brief Get pointer to free static memory,
     * please note, that we have no free, deallocate or similar.
     * @param bytes: number of bytes to be allocated
     * @return
     */
    static void* get(size_t bytes);



// custom member functions:
    /**
     * @brief get base address of static memory (according to C++ iterators)
     * @return
     */
    static void* begin();

    /**
     * @brief
     * @return pointer to byte beyond our memory (according to C++ iterators)
     */
    static void* end();

    /**
     * @brief remainingSize
     * @return number of elements of free memory remaining
     */
	static size_t remainingSize();

	/**
	 * @brief totalSize
	 * @return size of total memory statically allocated in bytes
	 */
	static size_t totalSize();

	/**
	 * @brief totalSize
	 * @return size of total memory allready dispensed in bytes
	 */
	static size_t dispensedSize();

	/**
     * @brief reset()
     * frees all memory, use with care!
     */
    static void reset();

private:
    static uint8_t* nextFreeAddress;
    static uint8_t* memory;
	static size_t size; // = MEMORY_DISPENSER_BYTES;
};








/**
 * @brief The StaticAllocator template class
 * complies with the pure abstraction as std::allocator
 *
 * this class has no free, since no memory should dynamically allocated and freed
 * in an embedded system, which is permanently on.
 *
 * It uses the static class MemoryDispenser
 *
 * see page 567, Stroustrup 2011
 */
template <typename T>
class StaticAllocator
{

public:
// type definitions to comply with std::allocator
    typedef T value_type;
    typedef size_t size_type;

#ifdef CODE_COMPOSER_STUDIO
	// CCS seems not to support the ptrdiff_t directly, we have to use std-namespace
	typedef std::ptrdiff_t difference_type;
#else
	typedef ptrdiff_t difference_type;
#endif

    typedef T* pointer;
    typedef const T* const_pointer;
    typedef T& reference;
    typedef const T& const_reference;



public:

// member functions to comply with std::allocator
    pointer address(reference r) const {return &r;}
    const_pointer address(const_reference r) const {return &r;}

    /**
     * @brief allocate: get pointer to free static memory
     * please note, that we have no free, deallocate or similar.
     * @param nElements
     * number of elements of type T to be allocated
     * @param hint
     * pointer, where the memory could or should be allocated.
     * @return
     */
    static T* allocate(size_type nElements, const void* hint=0);
    static void deallocate(pointer p, size_type n)
    {
        p++; n++; // remove warnings
        // nothing to do here, we only dispense memory, but do not take it back
    }

    /**
     * @brief construct: initialize an object, pointed to by p, with value val
     * @param p, pointer to uninitialized object
     * @param val, reference to source-value
     */
    void construct(pointer p, const T& val)
    {
        new(p) T(val); // placement new-operator, initialize *p by value val
    }

    /**
     * @brief destroy: call destructor of object, pointed to by p
     * @param p, pointer to object to be destructed
     */
    void destroy(pointer p)
    {
        p->~T();
    }

};





// Template Function:

    template<typename T>
    T* StaticAllocator<T>::allocate(size_type nElements, const void *hint)
    {
        int* unused = (int*)hint; unused++; // remove unused warning
        int bytes = nElements*sizeof(T);
        return (pointer)MemoryDispenser::get(bytes);
    }

    template<typename _T1, typename _T2>
      inline bool
      operator==(const StaticAllocator<_T1>&, const StaticAllocator<_T2>&)
      { return true; }

    template<typename _Tp>
      inline bool
      operator==(const StaticAllocator<_Tp>&, const StaticAllocator<_Tp>&)
      { return true; }

    template<typename _T1, typename _T2>
      inline bool
      operator!=(const StaticAllocator<_T1>&, const StaticAllocator<_T2>&)
      { return false; }

    template<typename _Tp>
      inline bool
      operator!=(const StaticAllocator<_Tp>&, const StaticAllocator<_Tp>&)
      { return false; }

#endif // MEMORYDISPENSER_H
