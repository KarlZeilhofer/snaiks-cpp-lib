
#include "div.h"
#include "snpin.h"
#include "snproperty.h"

SnDIV::SnDIV(int id, const char * const name, const char * const props)
	:SnObject(id, name)
{
	alloc(2,1,0,1);

	addInput(snaiksm(SnPin<snanalog_t>(11, &In1)));
	addInput(snaiksm(SnPin<snanalog_t>(12, &In2)));

	addOutput(snaiksm(SnPin<snanalog_t>(51, &Out)));

	addPropterty(snaiksm(SnProperty<snanalog_t>("DivByZeroValue", &DivByZeroValue)));

	DivByZeroValue = 0;
	setProps(props);
}

void SnDIV::update()
{
	if(In2 == 0){
		Out = DivByZeroValue;
	}else{
		Out = In1/In2;
	}
}

Sn::ObjType SnDIV::type()
{
	return Sn::DIV;
}

#include "div.h"
