#ifndef CONFIG_H
#define CONFIG_H



// Macros:
#ifndef PC_TEST
//#define ARDUINO_DEBUG
#endif


//#define USE_PLACEMENT_NEW // comment this line, if you want to use dynamic new operator
	// TODO 3: MemoryDispenser komplett deaktivieren!

#define SNAIKS_NAMELESS_OBJECTS // use this to save memory
#define SNAIKS_NAMELESS_PINS // use this to save memory
//#define SNAIKS_ALL_NONTRANSPARENT // use this to save memory


#define SNAIKS_ENABLE_UNIT_TESTS
// user has to call include "snunittest.h" and call SnUnitTest::run();

#endif // CONFIG_H
