#include "sum.h"

#ifndef SNAIKS_MAX_H_
#define SNAIKS_MAX_H_


// Output Maximum of 2 Inputsignals
class SnMAX:public SnSUM
{
public:
	SnMAX(int id, const char* const name=0);
	void update();
	Sn::ObjType type();
};


#endif
