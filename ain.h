#ifndef SnAIN_H
#define SnAIN_H

#include "snobject.h"

class SnAIN : public SnObject
{
public:
	SnAIN(int id, const char* const name=0, const  char* const props=0);
	void update();
	Sn::ObjType type();

	snanalog_t Out;
	snanalog_t InitValue;
};

#endif // SnAIN_H
