#include "aout.h"
#include "snpin.h"

SnAOUT::SnAOUT(int id, const char* const name)
	:SnObject(id, name)
{
	alloc(1,0,0,0);
	addInput(snaiksm(SnPin<snanalog_t>(11, &In)));
}

Sn::ObjType SnAOUT::type()
{
	return Sn::AOUT;
}
