#include "snobject.h"

#ifndef SNAIKS_DIF_H_
#define SNAIKS_DIF_H_


// Difference
class SnDIF:public SnObject
{
public:
	SnDIF(int id, const char* const name=0);
	void update();
	Sn::ObjType type();

	snanalog_t Plus;
	snanalog_t Minus;

	snanalog_t Out;
};


#endif
