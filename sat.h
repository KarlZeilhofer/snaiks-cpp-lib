#include "snobject.h"

#ifndef SNSAT_H
#define SNSAT_H


class SnSAT:public SnObject
{
public:
	SnSAT(int id, const char* const name=0, const char* const props=0);
	void update();
	Sn::ObjType type();

	snanalog_t In;
	snanalog_t Out;

	// Properties:
	snanalog_t Low;
	snanalog_t High;
};

#endif // SNSAT_H
