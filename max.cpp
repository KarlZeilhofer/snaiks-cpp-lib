#include "max.h"

SnMAX::SnMAX(int id, const char * const name)
	:SnSUM(id, name)
{
}

void SnMAX::update()
{
	if(In1 > In2){
		Out = In1;
	}else{
		Out = In2;
	}
}

Sn::ObjType SnMAX::type()
{
	return Sn::MAX;
}
