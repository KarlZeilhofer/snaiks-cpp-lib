#include "dand.h"
#include "dl.h"
#include "snpin.h"

SnDAND::SnDAND(int id, const char* const name)
	:SnObject(id, name)
{
	In1 = false;
	In2 = false;

	alloc(2,4,0,0);

	addInput(snaiksm(SnPin<bool>(11, &In1)));
	addInput(snaiksm(SnPin<bool>(12, &In2)));

	dl = snaiksm(SnDL(1));
	//addObj(dl); // don't add, since we update it manually

	// NOTE: this seems to be potentially dangerous, becaus whith the definition
	//		below, e.g. the Q-member of Digital Line dl gets a secod setter.
	//		when our outputpins of DAND are sampled, they could overwrite the results,
	//		calculated by Digital Line.
	//		BUT: the source of our output-pins is by default the address of the output-address
	//		so nothing happens, if this pins are sampled.
	addOutput(snaiksm(SnPin<bool>(51, &dl->Q)));
	addOutput(snaiksm(SnPin<bool>(52, &dl->rising)));
	addOutput(snaiksm(SnPin<bool>(53, &dl->falling)));
	addOutput(snaiksm(SnPin<bool>(54, &dl->Qn)));
}

void SnDAND::update()
{
	dl->In = In1 && In2;
	dl->update();
	// SnObject::update(); // sample output pins
		// we don't need to sample the outputs here...
}

Sn::ObjType SnDAND::type()
{
	return Sn::DAND;
}
